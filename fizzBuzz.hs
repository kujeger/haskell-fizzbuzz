module FizzBuzz where

fizzBuzz :: Int -> String
fizzBuzz x
    | mod x 3 == 0 && mod x 5 == 0 = "FizzBuzz"
    | mod x 3 == 0 = "Fizz"
    | mod x 5 == 0 = "Buzz"
    | otherwise = show x

fizzBuzz2 :: Int -> String
fizzBuzz2 x
    | null word = show x
    | otherwise = word
    where
        word = concat [ label | (y,label) <- fizs, mod x y == 0 ]
        fizs = [ (3,"Fizz"), (5,"Buzz") ]

main :: IO ()
main = do putStrLn $ unwords $ map fizzBuzz2 [-50..50]
